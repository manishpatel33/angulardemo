import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: any;
  password: any;
  constructor(private router: Router) {
    if (localStorage.getItem('userName')) {
      this.router.navigate(['/home']);
    }
  }

  ngOnInit(): void {
  }

  login() {
    if(this.email && this.password){
      localStorage.setItem('userName', this.email)
      localStorage.setItem('password', this.password)
      this.router.navigate(['/home']);
    }else{
      alert('please fill all required field')
    }
  }
}
