import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  profileName: any;
  isShowMore: boolean = false;
  constructor(private router: Router) {
    this.profileName = localStorage.getItem('userName')
    if (!this.profileName) {
      this.router.navigate(['/login']);
    }
  }

  dataObj = [
    { 'image': 'assets/8c82f5da07d18f591727fc4325134e0b.jpg', 'name': 'test 1!' },
    { 'image': 'assets/71zNWbTHzxL._SX679_.jpg', 'name': 'test 2@' },
    { 'image': 'assets/copy-space-roses-flowers_23-2148860032.jpg', 'name': 'test 3#' },
    { 'image': 'assets/dahlia-3598551_1920.jpg', 'name': 'test 4$' },
    { 'image': 'assets/download (1).jpg', 'name': 'test 5%' },
    { 'image': 'assets/download.jpg', 'name': 'test 6^' },
    { 'image': 'assets/flower-729512_960_720.jpg', 'name': 'test 7&' },
    { 'image': 'assets/pexels-photo-736230.jpeg', 'name': 'test 8*' },
  ];

  ngOnInit(): void {

  }

  logout() {
    localStorage.setItem('userName', "")
    localStorage.setItem('password', "")
    this.router.navigate(['/login']);
  }

}
